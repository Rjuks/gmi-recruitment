import mongoose from "mongoose";

const connectDB = async () => {
  if (mongoose.connections[0].readyState) {
    console.log("DB already connected");
    return;
  }

  if (!process.env.MONGODB_URI) {
    throw new Error("mongodbURI is not defined");
  }

  try {
    await mongoose.connect(process.env.MONGODB_URI);
    console.log("DB connected successfully");
  } catch (err) {
    console.log(err);
    throw new Error("Error while connecting a DB");
  }
};

export default connectDB;
