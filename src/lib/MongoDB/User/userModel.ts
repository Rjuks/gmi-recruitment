import { Model, Mongoose } from "mongoose";
import { UserDto } from "@/lib/zod/userModel";
import { generateRandomId } from "@/shared/utils";

const mongoose: Mongoose = require("mongoose");
const { Schema } = mongoose;

const userMongooseSchema = new Schema(
  {
    _id: {
      type: String,
      required: true,
      default: () => generateRandomId(),
    },
    username: { type: String, required: true },
    password: { type: String, required: true },
    avatar: { type: String, required: true },
  },
  { timestamps: false, versionKey: false }
);

let UserSchema: Model<UserDto>;

try {
  // @ts-ignore
  UserSchema = mongoose.model("Users");
} catch (e) {
  UserSchema = mongoose.model("Users", userMongooseSchema);
}

export default UserSchema;
