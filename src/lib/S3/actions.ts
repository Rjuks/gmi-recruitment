import { configS3 } from "@/lib/S3/configS3";

export const uploadImageToS3 = async (file: FileList) => {
  if (!file || !file[0]?.name) {
    return null;
  }

  const avatarFile = file[0];

  try {
    const uploadImage = configS3.upload({
      Bucket: process.env.NEXT_PUBLIC_S3_BUCKET_NAME as string,
      Key: avatarFile.name,
      Body: avatarFile,
    });

    const uploadResult = await uploadImage.promise();
    const imageURL = uploadResult.Location;

    console.log(`File uploaded successfully: ${avatarFile.name}`);
    return imageURL;
  } catch (err) {
    console.error(err);
    return null;
  }
};
