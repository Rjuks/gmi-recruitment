import { z } from "zod";

export interface User {
  _id: string;
  username: string;
  avatar: string;
}

export interface UserDto {
  _id: string;
  username: string;
  password: string;
  avatar: string;
}

export const userSchema = z.object({
  _id: z.string().nonempty("Username is required"),
  username: z.string().nonempty("Username is required"),
  avatar: z
    .string({ invalid_type_error: "Image is required" })
    .nonempty("Image is required"),
});

export const userRegisterSchema = z.object({
  username: z.string().nonempty("Username is required"),
  password: z.string().nonempty("Password is required"),
  avatar: z
    .string({ invalid_type_error: "Image is required" })
    .nonempty("Image is required"),
});

export const userLoginSchema = z.object({
  username: z.string().nonempty("Username is required"),
  password: z.string().nonempty("Password is required"),
});
