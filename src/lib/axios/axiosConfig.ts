import axios from "axios";

enum Environment {
  Development = "development",
  Production = "production",
  Preview = "preview",
}

export function getAppApiUrl(env?: string) {
  if (!env) {
    return "http://localhost:3000/api/";
  }
  if (env === Environment.Production) {
    return `https://${process.env.NEXT_PUBLIC_VERCEL_URL as string}/api/`;
  }
  if (env === Environment.Preview) {
    return `https://${process.env.NEXT_PUBLIC_VERCEL_URL as string}/api/`;
  }
  return "http://localhost:3000/api/";
}

export const axiosConfig = axios.create({
  baseURL: getAppApiUrl(process.env.NEXT_PUBLIC_VERCEL_ENV as string),
});
