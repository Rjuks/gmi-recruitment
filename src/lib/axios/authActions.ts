import { FieldValues } from "react-hook-form";
import { axiosConfig } from "@/lib/axios/axiosConfig";
import { AxiosError } from "axios";
import { User, userSchema } from "@/lib/zod/userModel";

export const registerUserHandler = async (
  formData: FieldValues
): Promise<{ message: string } | AxiosError> => {
  try {
    const { data } = await axiosConfig.post<Promise<{ message: string }>>(
      "user/register",
      formData
    );

    return data;
  } catch (error: any) {
    if (error.isAxiosError) {
      throw new Error(error.response.data.message);
    } else {
      throw new Error(
        "An error occurred during registration. Please try again."
      );
    }
  }
};

export const loginUserHandler = async (
  formData: FieldValues
): Promise<{ data: User }> => {
  try {
    const { data } = await axiosConfig.post<
      Promise<{ data: User; message: string }>
    >("user/login", formData);

    return data;
  } catch (error: any) {
    if (error.isAxiosError) {
      throw new Error(error.response.data.message);
    } else {
      throw new Error(
        "An error occurred during registration. Please try again."
      );
    }
  }
};
