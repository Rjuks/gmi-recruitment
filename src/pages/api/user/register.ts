import { NextApiRequest, NextApiResponse } from "next";
import UserSchema from "@/lib/MongoDB/User/userModel";
import connectDB from "@/lib/MongoDB/connectDB";
import { generateRandomId } from "@/shared/utils";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method === "POST") {
    try {
      await connectDB();

      const { username, password, avatar } = req.body;

      const userDB = await UserSchema.findOne({ username });

      if (userDB) {
        return res
          .status(400)
          .json({ message: "User with this username already exists" });
      }

      const newUser = new UserSchema({
        username,
        password,
        avatar,
      });

      await newUser.save();

      res.status(201).json({ message: "User created successfully" });
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: "Internal server error" });
    }
  } else {
    res.status(405).json({ message: "Method Not Allowed" });
  }
}
