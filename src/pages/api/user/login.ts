import { NextApiRequest, NextApiResponse } from "next";
import UserSchema from "@/lib/MongoDB/User/userModel";
import connectDB from "@/lib/MongoDB/connectDB";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method === "POST") {
    try {
      await connectDB();

      const { username, password } = req.body;

      const userDB = await UserSchema.findOne({ username });

      if (!userDB) {
        return res
          .status(401)
          .json({ message: "Invalid username or password" });
      }

      // todo - password should be decrypted
      const isPasswordValid = userDB.password === password;

      if (!isPasswordValid) {
        return res
          .status(401)
          .json({ message: "Invalid username or password" });
      }

      res.status(200).json({
        message: "Login successful",
        data: {
          username: userDB.username,
          avatar: userDB.avatar,
          _id: userDB._id,
        },
      });
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: "Internal server error" });
    }
  } else {
    res.status(405).json({ message: "Method Not Allowed" });
  }
}
