import Head from "next/head";
import styles from "@/styles/Home.module.css";
import { RegisterPage } from "@/components/RegisterPage/RegisterPage";

export default function Home() {
  return (
    <>
      <Head>
        <title>GMI Recruitment - register</title>
        <meta name="description" content="Application for recruitment needs" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className={styles.main}>
        <RegisterPage />
      </main>
    </>
  );
}
