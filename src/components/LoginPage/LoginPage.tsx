import { LoginForm } from "@/components/UI/Forms/AuthForms/LoginForm/LoginForm";

export const LoginPage = () => {
  return (
    <>
      <LoginForm />
    </>
  );
};
