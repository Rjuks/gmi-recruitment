import { RegisterForm } from "@/components/UI/Forms/AuthForms/RegisterForm/RegisterForm";

export const RegisterPage = () => {
  return (
    <>
      <RegisterForm />
    </>
  );
};
