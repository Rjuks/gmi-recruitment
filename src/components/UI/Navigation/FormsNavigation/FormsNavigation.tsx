import React from "react";
import styles from "./FormsNavigation.module.css";
import Link from "next/link";
import { ROUTES } from "@/consts/routes";

interface FormsNavigationProps {
  route: ROUTES;
}

export const FormsNavigation = ({ route }: FormsNavigationProps) => {
  const renderCurrentRouteInfo = () => {
    switch (route) {
      case ROUTES.LOGIN:
        return (
          <>
            <span className={styles.info}>Already have an account? </span>
            <Link href={ROUTES.LOGIN} className={styles.redirectLink}>
              Sign in
            </Link>
          </>
        );
      case ROUTES.REGISTER:
        return (
          <>
            <span>Don&apos;t have an account?</span>
            <Link href={ROUTES.REGISTER} className={styles.redirectLink}>
              Sign up
            </Link>
          </>
        );
      default:
        return null;
    }
  };

  return <div className={styles.container}>{renderCurrentRouteInfo()}</div>;
};
