import React, { InputHTMLAttributes, forwardRef } from "react";
import styles from "./FileInput.module.css";
import { FieldError, FieldErrorsImpl, Merge } from "react-hook-form";
import uploadFile from "../../../../shared/assets/uploadFile.svg";
import Image from "next/image";

interface FileInputProps extends InputHTMLAttributes<HTMLInputElement> {
  label: string;
  name: string;
  errorMessage?: string | FieldError | Merge<FieldError, FieldErrorsImpl<any>>;
}

export const FileInput = forwardRef<HTMLInputElement, FileInputProps>(
  ({ label, name, errorMessage, ...props }, ref) => (
    <section className={`${styles.inputContainer}`}>
      <div className={styles.uploadContent}>
        <Image
          src={uploadFile}
          alt="upload file"
          className={styles.uploadContent_image}
        />
        <span className={styles.uploadContent_dragInfo}>
          Drag & drop any image here
        </span>
        <span className={styles.uploadContent_browseInfo}>
          Click to browse file from device
        </span>
      </div>

      <label htmlFor={name}>{label}</label>
      <input
        ref={ref}
        className={styles.input}
        id={name}
        name={name}
        type="file"
        {...props}
      />
      {errorMessage && typeof errorMessage === "string" && (
        <p role="alert" className={styles.errorMessage}>
          {errorMessage}
        </p>
      )}
    </section>
  )
);

FileInput.displayName = "FileInput";
