import React, { InputHTMLAttributes, forwardRef } from "react";
import styles from "./TextInput.module.css";
import { FieldError, FieldErrorsImpl, Merge } from "react-hook-form";

interface InputTextProps extends InputHTMLAttributes<HTMLInputElement> {
  label: string;
  name: string;
  errorMessage?: string | FieldError | Merge<FieldError, FieldErrorsImpl<any>>;
}

export const InputText = forwardRef<HTMLInputElement, InputTextProps>(
  ({ label, name, errorMessage, ...props }, ref) => (
    <section
      className={`${styles.inputContainer} ${
        errorMessage ? styles.invalid : ""
      }`}
    >
      <div>
        <label htmlFor={name}>{label}</label>
        <input
          ref={ref}
          className={styles.input}
          id={name}
          name={name}
          {...props}
        />
      </div>
      {errorMessage && typeof errorMessage === "string" && (
        <p role="alert" className={styles.errorMessage}>
          {errorMessage}
        </p>
      )}
    </section>
  )
);

InputText.displayName = "InputText";
