import React, { useState } from "react";
import { FieldValues, useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import styles from "./LoginForm.module.css";
import { User, userLoginSchema, userSchema } from "@/lib/zod/userModel";
import { InputText } from "@/components/UI/Inputs/TextInput/TextInput";
import { Button } from "@/components/UI/Button/Button";
import { FormsNavigation } from "@/components/UI/Navigation/FormsNavigation/FormsNavigation";
import { ROUTES } from "@/consts/routes";
import { loginUserHandler } from "@/lib/axios/authActions";

export const LoginForm = () => {
  const [userData, setUserData] = useState<User | null>(null);
  const {
    handleSubmit,
    register,
    formState: { errors },
    setError,
  } = useForm({
    resolver: zodResolver(userLoginSchema),
  });

  const onSubmit = async (formData: FieldValues) => {
    setUserData(null);
    try {
      const { data } = await loginUserHandler(formData);

      const parsedUser = userSchema.parse(data);

      setUserData(parsedUser);
    } catch (error: any) {
      setError("api", {
        message: error?.message || "",
      });
    }
  };

  return (
    <section className={styles.container}>
      <h2 className={styles.formHeader}>Login Form</h2>
      <form onSubmit={handleSubmit(onSubmit)} className={styles.form}>
        <InputText
          label="Username"
          errorMessage={errors.username ? errors.username.message : ""}
          {...register("username")}
        />
        <InputText
          label="Password"
          errorMessage={errors.password && errors.password.message}
          type="password"
          {...register("password")}
        />

        {errors.api && errors.api.message && (
          <p role="alert" className={styles.errorMessage}>
            {`${errors.api.message}`}
          </p>
        )}

        <div className={styles.buttonsContainer}>
          <Button type="submit">Log In</Button>
        </div>
      </form>

      {userData && <pre>{JSON.stringify(userData, null, 2)}</pre>}

      <FormsNavigation route={ROUTES.REGISTER} />
    </section>
  );
};
