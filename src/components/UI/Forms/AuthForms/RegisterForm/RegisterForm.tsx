import React, { useState } from "react";
import { FieldValues, useForm } from "react-hook-form";
import styles from "./RegisterForm.module.css";
import { zodResolver } from "@hookform/resolvers/zod";
import { userRegisterSchema } from "@/lib/zod/userModel";
import { uploadImageToS3 } from "@/lib/S3/actions";
import { InputText } from "@/components/UI/Inputs/TextInput/TextInput";
import { FileInput } from "@/components/UI/Inputs/FileInput/FileInput";
import { Button } from "@/components/UI/Button/Button";
import { FormsNavigation } from "@/components/UI/Navigation/FormsNavigation/FormsNavigation";
import { ROUTES } from "@/consts/routes";
import { registerUserHandler } from "@/lib/axios/authActions";

export const RegisterForm = () => {
  const [uploadedImg, setUploadedImg] = useState("");
  const {
    handleSubmit,
    register,
    formState: { errors },
    getValues,
    setValue,
    watch,
    clearErrors,
    setError,
  } = useForm({
    resolver: zodResolver(userRegisterSchema),
  });

  const handleRemoveImg = () => {
    setValue("avatar", "");
    setUploadedImg("");
  };

  const onSubmit = async (formData: FieldValues) => {
    try {
      const response = await registerUserHandler(formData);

      window.alert(response.message);
    } catch (error: any) {
      setError("api", {
        message: error?.message || "",
      });
    }
  };

  const handleUpload = async () => {
    const getUploadedImg = await uploadImageToS3(getValues().avatar);

    setValue("avatar", getUploadedImg);
    clearErrors("avatar");
    setUploadedImg(getUploadedImg || "");
  };

  const avatarFileName = watch().avatar?.[0]?.name || "";

  return (
    <section className={styles.container}>
      <h2 className={styles.formHeader}>Register Form</h2>
      <form onSubmit={handleSubmit(onSubmit)} className={styles.form}>
        <InputText
          label="Username"
          errorMessage={errors.username && errors.username.message}
          {...register("username")}
        />
        <InputText
          label="Password"
          errorMessage={errors.password && errors.password.message}
          type="password"
          {...register("password")}
        />
        <FileInput
          label={avatarFileName}
          errorMessage={errors.avatar && errors.avatar.message}
          {...register("avatar")}
        />

        {uploadedImg && (
          <div className={styles.uploadedImgContainer}>
            <div className={styles.uploadedImageWrapper}>
              <img src={uploadedImg} alt="image" width={200} height={200} />
              <button className={styles.removeButton} onClick={handleRemoveImg}>
                X
              </button>
            </div>
          </div>
        )}

        <div className={styles.buttonsContainer}>
          <Button
            type="button"
            onClick={handleUpload}
            disabled={!avatarFileName}
          >
            Upload
          </Button>

          {errors.api && errors.api.message && (
            <p role="alert" className={styles.errorMessage}>
              {`${errors.api.message}`}
            </p>
          )}

          <Button type="submit">Submit</Button>
        </div>
      </form>

      <FormsNavigation route={ROUTES.LOGIN} />
    </section>
  );
};
