import { v4 as randomId } from "uuid";

export const generateRandomId = () => randomId();
