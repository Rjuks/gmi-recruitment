export enum ROUTES {
  "HOME" = "/home",
  "LOGIN" = "/login",
  "REGISTER" = "/register",
}
