## GMI recruitment task 📰

📌 The main purpose of this project is to provide a user registration system with the ability to upload avatars. Users can register by 
providing their desired username and password, along with an avatar image. The avatar image is then uploaded to an S3 bucket for storage 
and saved in MongoDB with user credentials.

## Technologies

- ReactJS
- NextJS 
- TypeScript
- AWS SDK
- MongoDB + mongoose
- Axios
- ZOD
- React Hook Form

- ESlint
- Prettier


## Features

- User can register
- User can log in
- Saving the user in the MongoDB database
- Saving the user's avatar in AWS S3
- Form validations
- Request handling


## Run Locally

- Clone repository

```
  git clone https://gitlab.com/Rjuks/gmi-recruitment.git
```

ℹ️ Instructions for running client app locally:

```bash
  npm install
```

- Run client app in development mode

```bash
  npm run dev
```



## Environment Variables

⚙️ You need these environment variables for the application to work properly

- `MONGODB_URI`: The connection string for MongoDB database
- `NEXT_PUBLIC_S3_ACCESS_KEY_ID`: The access key ID for S3 bucket
- `NEXT_PUBLIC_S3_SECRET_ACCESS_KEY`: The secret access key for S3 bucket.
- `NEXT_PUBLIC_S3_BUCKET_NAME`: The name of S3 bucket.



## Feedback

- Lack of visual aspect
- No support for password hashing
- No proper data validation on the backend side
- Lack of UI notifications for client-side after successful requests or errors

## Authors

- [@Karol Opólko](https://gitlab.com/Rjuks)

